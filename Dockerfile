FROM node:carbon

# Create app directory
WORKDIR /usr/src/app

RUN apt-get update && \
    apt-get install -y git
        
# Authorize SSH Host
RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan gitlab.com > /root/.ssh/known_hosts
    
COPY ssh_keys/id_rsa.pub /root/.ssh/id_rsa.pub 
COPY ssh_keys/id_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa.pub

COPY app/package*.json ./

RUN npm install

COPY app/ ./

RUN git config --global user.email "info@tzm.community"
RUN git config --global user.name "TZM Discord bot"

RUN mkdir -p /usr/src/news/published/items

RUN mkdir -p /usr/src/news/logs
COPY publish.sh /usr/src/news/publish.sh

CMD ["node", "server.js"]
