#!/bin/bash

cd /usr/src/news

log=/usr/src/news/logs/log.log

curl localhost:18881/news

cd /usr/src/news

if [ -d chapter-site-root ]; then
	yes | rm -r chapter-site-root > /dev/null 2>&1
fi

git clone git@gitlab.com:tzmcommunity/chapter-site-root.git
find
cd chapter-site-root

if [ -d _posts ]; then
	cp /usr/src/news/news/_posts/*.md _posts
    cp /usr/src/news/published/items/*.md _posts 
    cp /usr/src/news/published/*.json .

	git diff
    git add . -A 
    git commit -m "TZM News bot" 
    git push 
fi

